class ChatbotService
  attr_accessor :params, :choosed_query

  def initialize(request, choosed_query = nil)
    @params = request.params
    @choosed_query = choosed_query
  end

  def execute
    if params["hub.mode"] == "subscribe" && params["hub.verify_token"] == AppConfig.verify_token
      params["hub.challenge"]
    else
      send_message
    end
  end

  def get_book_per_id(recipient_id, id)
    message = "Kindly, Choose The Book or Similar Book"
    xpath = "book"
    books_response = Nokogiri::XML(HTTParty.get("https://www.goodreads.com/book/show/#{id}.xml?key=#{AppConfig.goodreads_key}").body)
    post_goodreads_response recipient_id, message, books_response, xpath
  end

  def get_book_per_name(recipient_id, name)
    message = "Kindly, Choose Book"
    xpath = "best_book"
    books_response = Nokogiri::XML(HTTParty.get("https://www.goodreads.com/search.xml?key=#{AppConfig.goodreads_key}&q=#{name}").body)
    post_goodreads_response recipient_id, message, books_response, xpath
  end

  private

  def send_message
    params[:entry].each do |entry|
      entry[:messaging].each do |messaging|
        if messaging[:message].present?
          sender_id = messaging[:sender][:id]
          check_message_format sender_id, messaging[:message][:text]
        elsif messaging[:postback].present?
          sender_id = messaging[:sender][:id]
          payload = messaging[:postback][:payload]
          postback_message(sender_id, payload)
        end
      end
    end
    "Done"
  end

  def check_message_format(sender_id, text_message)
    try(:"get_book_per_#{text_message.split[0].downcase}", sender_id, text_message.split.drop(1).join(" ")) || welcome_message(sender_id)
  end

  def welcome_message(recipient_id)
    text_message = "Welcome #{user_data(recipient_id)["first_name"]},\n
              Please Choose whether if you would like to search for books per ID or Name\n
              Kindly, writ it with the formats below\n
              ID {id} OR Name {name}\n
              Ex: Name end game\n
                  ID 375802"
    send_text_message(recipient_id, text_message)
  end

  def user_data(recipient_id)
    JSON.parse(HTTParty.get("https://graph.facebook.com/v2.6/#{recipient_id}?access_token=#{AppConfig.page_access_token}").body)
  end

  def send_text_message(recipient_id, text_message)
    message = { text: text_message }
    post_message(recipient_id, message)
  end

  def post_message(recipient_id, message)
    HTTParty.post("https://graph.facebook.com/v2.6/me/messages?access_token=#{AppConfig.page_access_token}",
      body: {
        recipient: {
          id: recipient_id
        },
        message: message
      }.to_json,
      headers: { 'Content-Type' => 'application/json' } )
  end

  def post_goodreads_response(recipient_id, text_message, books_response, xpath)
    message = {
      attachment: {
        type: "template",
        payload: {
          template_type: "button",
          text: text_message,
          buttons: []
        }
      }
    }
    books_response.xpath("//#{xpath}").first(3).each do |config|
      message[:attachment][:payload][:buttons] << {type: "postback", title: config.xpath("title").text, payload: config.xpath("id").text}
    end
    post_message(recipient_id, message)
    message[:attachment][:payload][:buttons] = []
    books_response.xpath("//#{xpath}").first(5).last(2).each do |config|
      message[:attachment][:payload][:buttons] << {type: "postback", title: config.xpath("title").text, payload: config.xpath("id").text}
    end
    post_message(recipient_id, message)
  end

  def postback_message(recipient_id, payload)
    send_text_message(recipient_id, "Recommended or Not")
  end

end
