class ChatbotController < ApplicationController
skip_before_action :verify_authenticity_token, only: [:callback]

  def callback
    callback_response = ChatbotService.new(request).execute
    render plain: callback_response
    response.body
    response.content_type
  end

end
