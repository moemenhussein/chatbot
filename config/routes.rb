Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  match "chatbot/callback", to: "chatbot#callback", via: [:get, :post]
end
